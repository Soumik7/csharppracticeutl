﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld
{
    class Program
    {

        //always keep the main function clean. avoid writing any code other than calling functions and creating object instances
        static void Main(string[] args)
        {
            var functionname = "Main(string[] args)";
            string displaymessageenter = "--------entering------- " + functionname;
            string displaymessageleave = "--------leaving-------" + functionname;
            DisplayStuff(displaymessageenter);

            #region main code stuff.
            BasicInputAndOutput();

            //Four Types Of Functions 

            string name = "Soumik Naskar";
            //Send Paramters and Recieve return
            string response = SendAndRecieve(name);

            //Send Parameters and Received nothing
            SendOnly(name);
            //Send nothing and recieve return
            response = ReceiveOnly();

            //Send nothinig nad receive nothing. 
            NothingFunction();

            #endregion

            DisplayStuff(displaymessageleave);
            Console.ReadLine();
        }

        private static void DisplayStuff(string displaymessage)
        {
            //show a message on the screen;
            Console.WriteLine(displaymessage);
        }

        #region functions with basics
        //this function takes nothing as parameters
        //returns nothing 
        private static void NothingFunction()
        {
            var functionname = "NothingFunction()";
            string displaymessageenter = "--------entering------- " + functionname;
            string displaymessageleave = "--------leaving-------" + functionname;
            DisplayStuff(displaymessageenter);
            //this is nothing. 
            var dummy = "";
            DisplayStuff(displaymessageleave);
        }

        //this functions takes no parameters
        //but returns a simple string 
        private static string ReceiveOnly()
        {
            var functionname = "ReceiveOnly()";
            string displaymessageenter = "--------entering------- " + functionname;
            string displaymessageleave = "--------leaving-------" + functionname;
            DisplayStuff(displaymessageenter);
            string someresponsestring = "Batman";
            DisplayStuff(displaymessageleave);
            return someresponsestring;
        }

        //this function receives a string
        // but does not return anything
        private static void SendOnly(string somestring)
        {
            var functionname = "SendOnly(string somestring)";
            string displaymessageenter = "--------entering------- " + functionname;
            string displaymessageleave = "--------leaving-------" + functionname;
            DisplayStuff(displaymessageenter);
            //do nothing.
            var dummy = "";
            DisplayStuff(displaymessageleave);
        }

        //this function takes something and
        //also returns something. 
        private static string SendAndRecieve(string name)
        {
            var functionname = "SendAndRecieve(string somestring)";
            string displaymessageenter = "--------entering------- " + functionname + "the name which we got is " +name;
            string displaymessageleave = "--------leaving-------" + functionname;
            DisplayStuff(displaymessageenter);
            string temp = ""+name+"the name is the greatest name in the world";
            string temp2 = "i was just kidding ";
            string temp3 = temp + temp2;
            DisplayStuff(temp3); 
            string someresponsestring = "Batman";
            DisplayStuff(displaymessageleave);
            return someresponsestring;
        }

        //this a function that does some basic stuff
        private static void BasicInputAndOutput()
        {
            var functionname = "BasicInputAndOutput()";
            string displaymessageenter = "--------entering------- " + functionname;
            string displaymessageleave = "--------leaving-------" + functionname;
            DisplayStuff(displaymessageenter);
            //create two string variables and assigning them some values. 
            var message = "hello world";
            var message2 = "Ameesha Patel is the greatest half decent actress in the world";
            //building the final output message. and also do some essential formatting so it looks good.
            var outputmessage = message + ".\n" + message2;
            //print the output message.
            Console.WriteLine(outputmessage);
            //put this so that the program will not exit and keep the output window. 
            //the moment any key is pressed, the program will exist. 

            //lets create three new variables
            int somenumber = 5;
            double somedoublenumber = 5.5;
            string somestring = "I'm going to make him an offer he can't refuse";
            outputmessage = "\nThis is the number - " + somenumber +
                            "\nThis is the floating value - " + somedoublenumber +
                            "\nThis is the sentence from string - " + somestring;
            //print the output message.
            Console.WriteLine(outputmessage);
            DisplayStuff(displaymessageleave);
        }

        #endregion
    }
}
